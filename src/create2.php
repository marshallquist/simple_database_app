<!doctype html>
<html lang="en">
<head>



<?php
if (isset($_POST['submit'])) {
    require "config.php";
    require "common.php";
  
    try {
        $connection = new PDO($dsn, $username, $password, $options);

        $new_user = array(
          "firstname" => $_POST['firstname'],
          "lastname"  => $_POST['lastname'],
          "email"     => $_POST['email'],
          "age"       => $_POST['age'],
          "location"  => $_POST['location']
        );
        
        $sql = sprintf(
            "INSERT INTO %s (%s) values (%s)",
            "users",
            implode(", ", array_keys($new_user)),
            ":" . implode(", :", array_keys($new_user))
        );
        
        $statement = $connection->prepare($sql);
        $statement->execute($new_user);
  
    } catch(PDOException $error) {
      echo $sql . "<br>" . $error->getMessage();
    }
  
  }

?>
<?php include "templates/header.php"; ?>

<?php if (isset($_POST['submit']) && $statement) { ?>
  <?php echo escape($_POST['firstname']); ?> successfully added.
<?php } ?>



  <meta charset="utf-8">
  <meta firstname="viewport" content="width=device-width, initial-scale=1">
  <title>jQuery UI Dialog - Modal form</title>
  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <link rel="stylesheet" href="/resources/demos/style.css">
  <style>
    label, input { display:block; }
    input.text { margin-bottom:12px; width:95%; padding: .4em; }
    fieldset { padding:0; border:0; margin-top:25px; }
    h1 { font-size: 1.2em; margin: .6em 0; }
    div#users-contain { width: 350px; margin: 20px 0; }
    div#users-contain table { margin: 1em 0; border-collapse: collapse; width: 100%; }
    div#users-contain table td, div#users-contain table th { border: 1px solid #eee; padding: .6em 10px; text-align: left; }
    .ui-dialog .ui-state-error { padding: .3em; }
    .validateTips { border: 1px solid transparent; padding: 0.3em; }
  </style>
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <script>
  $( function() {
    var dialog, form,
 
      // From http://www.whatwg.org/specs/web-apps/current-work/multipage/states-of-the-type-attribute.html#e-mail-state-%28type=email%29
      emailRegex = /^[a-zA-Z0-9.!#$%&'*+\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/,
      firstname = $( "#firstname" ),
      lastname = $( "#lastname"),
      email = $( "#email" ),
      age = $( "#age" ),
      location = $( "#location"),
      allFields = $( [] ).add( firstname ).add( lastname ).add( email ).add( age ).add( location),
      tips = $( ".validateTips" );
 
    function updateTips( t ) {
      tips
        .text( t )
        .addClass( "ui-state-highlight" );
      setTimeout(function() {
        tips.removeClass( "ui-state-highlight", 1500 );
      }, 500 );
    }
 
    function checkLength( o, n, min, max ) {
      if ( o.val().length > max || o.val().length < min ) {
        o.addClass( "ui-state-error" );
        updateTips( "Length of " + n + " must be between " +
          min + " and " + max + "." );
        return false;
      } else {
        return true;
      }
    }
 
    function checkRegexp( o, regexp, n ) {
      if ( !( regexp.test( o.val() ) ) ) {
        o.addClass( "ui-state-error" );
        updateTips( n );
        return false;
      } else {
        return true;
      }
    }
 
    function addUser() {
      var valid = true;
      allFields.removeClass( "ui-state-error" );
 
      valid = valid && checkLength( firstname, "firstname", 1, 30 );
      valid = valid && checkLength( lastname, "lastname", 1, 30 );
      valid = valid && checkLength( email, "email", 6, 80 );
      valid = valid && checkLength( age, "age", 1, 10 );
      valid = valid && checkLength( location, "location", 1, 30 );
 
      valid = valid && checkRegexp( firstname, /^[a-z]([0-9a-z_\s])+$/i, "Username may consist of a-z, 0-9, underscores, spaces and must begin with a letter." );
      valid = valid && checkRegexp( email, emailRegex, "eg. ui@jquery.com" );
      valid = valid && checkRegexp( age, /^([0-9a-zA-Z])+$/, "Password field only allow : a-z 0-9" );
 
      if ( valid ) {
        var user = [firstname.val(), lastname.val(), email.val(), age.val(), location.val()];
        $.ajax({
            url : 'create3.php',
            method : 'POST',
            data:{
                arrayData:user
            },
            success : function(response)
            {
                alert("data sent response is "+response);
            },
            error : function(e)
            {
                alert("data not sent")
            }
            });
        dialog.dialog( "close" );
      }
      return valid;
    }

    
 
    dialog = $( "#dialog-form" ).dialog({
      autoOpen: false,
      height: 400,
      width: 350,
      modal: true,
      buttons: {
        "Submit": addUser,
        Cancel: function() {
          dialog.dialog( "close" );
        }
      },
      close: function() {
        form[ 0 ].reset();
        allFields.removeClass( "ui-state-error" );
      }
    });
 
    form = dialog.find( "form" ).on( "submit", function( event ) {
      event.preventDefault();
      addUser();
    });
 
    $( "#create-user" ).button().on( "click", function() {
      dialog.dialog( "open" );
    });
  } );
  </script>
</head>
<body>
 
<div id="dialog-form" title="Create new user">
 
  <form method = "post">
    <fieldset>
      <label for="firstname">First Name</label>
      <input type="text" name="firstname" id="firstname" class="text ui-widget-content ui-corner-all">
      <label for="lastname">Last Name</label>
      <input type="text" name="lastname" id="lastname" class="text ui-widget-content ui-corner-all">
      <label for="email">Email</label>
      <input type="text" name="email" id="email" class="text ui-widget-content ui-corner-all">
      <label for="age">Age</label>
      <input type="age" name="age" id="age" class="text ui-widget-content ui-corner-all">
      <label for="location">Location</label>
      <input type="text" name="location" id="location" class="text ui-widget-content ui-corner-all">
 
      <!-- Allow form submission with keyboard without duplicating the dialog button -->
      <input type="submit" tabindex="-1" style="position:absolute; top:-1000px">
    </fieldset>
  </form>
</div>
 



 
<button id="create-user">Create new user</button>
 
 
</body>
</html>


<a href="index.php">Back to home</a>

    <?php include "templates/footer.php"; ?>