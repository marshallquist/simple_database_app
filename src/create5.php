<?php
//file for handling delete user ajax from index.php




//read in ID 
$Data = $_REQUEST['Data'];
echo($Data);

require "config.php";
require "common.php";

  try {
    $connection = new PDO($dsn, $username, $password, $options);

    $id = $Data;

    $sql = "DELETE FROM users WHERE id = :id";

    $statement = $connection->prepare($sql);
    $statement->bindValue(':id', $id);
    $statement->execute();

    $success = "User successfully deleted";
  } catch(PDOException $error) {
    echo $sql . "<br>" . $error->getMessage();
  }
