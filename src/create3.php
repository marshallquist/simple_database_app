<?php
//file for handling create new user ajax from index.php


//Request data in an array

$arrayData = $_REQUEST['arrayData'];
foreach($arrayData as $data)
{
    echo $data;
}


//try catch blocks with sql statements to put data into database
try {
    require "config.php";
    require "common.php";
    $connection = new PDO($dsn, $username, $password, $options);

    $new_user = array(
      "img" => $arrayData[0],  
      "firstname" => $arrayData[1],
      "lastname"  => $arrayData[2],
      "email"     => $arrayData[3],
      "age"       => $arrayData[4],
      "location"  => $arrayData[5],
      "lat" => $arrayData[6],
      "lon" => $arrayData[7]
    );
    
    $sql = sprintf(
        "INSERT INTO %s (%s) values (%s)",
        "users",
        implode(", ", array_keys($new_user)),
        ":" . implode(", :", array_keys($new_user))
    );
    
    $statement = $connection->prepare($sql);
    $statement->execute($new_user);

} catch(PDOException $error) {
  echo $sql . "<br>" . $error->getMessage();
}




?>
  