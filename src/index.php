<?php require "templates/static.php"; ?>

<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBYOy-e5TExW85FLdcS25AJuh515XB_MEo&callback=initMap">
    </script>




<!-- Html table setup -->

<table id ="list" class = "display">
  <thead>
    <tr>
      <th>#</th>
      <th>Profile Picture</th>
      <th>First Name</th>
      <th>Last Name</th>
      <th>Email Address</th>
      <th>Age</th>
      <th>Location</th>
      <th>Date</th>
      <th>Latitude</th>
      <th>Longitude</th>
    </tr>
  </thead>
</table>
<!--Jquery inclusions -->
<script src="https://code.jquery.com/jquery-3.3.1.js"></script>
<script  src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script  src="https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js"></script>
<script  src="https://cdn.datatables.net/select/1.3.0/js/dataTables.select.min.js"></script>
<script  src="../../extensions/Editor/js/dataTables.editor.min.js"></script>



<script>



 



var e; //global variable to store id;
var lattt; //empty arrays to store all lat and lon values
var lonnn;
var rows; //global variable to store updated # of rows
var addlat;
var addlon;




//turning html table into jquery datatable

  $(function() {
    var table = $('#list').DataTable( {
      //"processing": true,
      //"serverSide": true,
      "ajax": {
        "url": "http://localhost:8000/api/v1/employees",
        
      },
      "columns": [
        { "data": "id" },
        { "data": "img" },
        { "data": "firstname" },
        { "data": "lastname" },
        { "data": "email" },
        { "data": "age" },
        { "data": "location" },
        { "data": "date" },
        { "data": "lat"},
        { "data": "lon"}
      ],
      "columnDefs": [
        {
          "targets": 1,
          "render": function(data){
            return '<img src = "'+data+'" width = "40" height = "50">'
          }
        }
      ]
      
       
    } );
    //get number of rows and store lat and lon data in global arrays
    
    $(function() {
      rows = table.rows().count();
      var i;
      var latitude = [];
      var longitude = [];

      for (i = 0; i < rows; i++){
        var r = table.row( i ).data();
      latitude.push(r.lat);
      longitude.push(r.lon);
      }
      lattt = latitude;
      lonnn = longitude;
    });
    


    $('#list').on( 'click', 'tbody tr', function () {
      var data = table.row( this ).data();
      var id = data.id;
      e=id
      var url = "create4.php";
      
        //alert( 'You clicked on '+data.firstname+'\'s row' );
        $('#edit-form').dialog('open');
        
    } );
    //opening the dialog form to edit when a row is clicked on
    $('#list').on( 'click', 'tbody tr', function () {
      var data = table.row( this ).data();
      var id = data.id;
      var url = "create4.php";
      
        //alert( 'You clicked on '+data.firstname+'\'s row' );
        $('#edit-form').dialog('open');
        
    } );

} );




                        //Editing dialog form

$( function() {
    var edit, form,
 
      // From http://www.whatwg.org/specs/web-apps/current-work/multipage/states-of-the-type-attribute.html#e-mail-state-%28type=email%29
      emailRegex = /^[a-zA-Z0-9.!#$%&'*+\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/,
      img = $( "#img" ),
      firstname = $( "#firstname" ),
      lastname = $( "#lastname"),
      email = $( "#email" ),
      age = $( "#age" ),
      location = $( "#location"),
      lat = $( "#lat"),
      lon = $( "#lon"),
      allFields = $( [] ).add( img ).add( firstname ).add( lastname ).add( email ).add( age ).add( location).add( lat ).add( lon ),
      tips = $( ".validateTips" );
 
 //not 100% sure what this function does but it was in the example and breaks the form when not present
    function updateTips( t ) {
      tips
        .text( t )
        .addClass( "ui-state-highlight" );
      setTimeout(function() {
        tips.removeClass( "ui-state-highlight", 1500 );
      }, 500 );
    }
 
   
 
    
 //function that sends edited data to database
    function editUser() {
      var valid = true;
      allFields.removeClass( "ui-state-error" );
 
 
      if ( valid ) {
        var user = [e, img.val(), firstname.val(), lastname.val(), email.val(), age.val(), location.val(), addlat, addlon];
        $.ajax({
            url : 'create4.php',
            method : 'POST',
            data:{
                arrayData:user
            },
            success : function(response)
            {
                alert("data sent response is "+response);
            },
            error : function(e)
            {
                alert("data not sent")
            }
            });
        edit.dialog( "close" );
      }
      new google.maps.Marker({position: {lat: lat.val(), lng:lon.val()}, map: map});
      return valid;
    }

    
 //dialog form buttons and style

    edit = $( "#edit-form" ).dialog({
      autoOpen: false,
      height: 400,
      width: 350,
      modal: true,
      buttons: {
        "Submit": editUser,
        Cancel: function() {
          edit.dialog( "close" );
        },
        "Delete User": function(){
          var delt = e;
          
          console.log(delt);
        $.ajax({
            url : 'create5.php',
            method : 'POST',
            data:{
                Data:delt
            },
            success : function(response)
            {
                alert("data sent response is "+response);
            },
            error : function(e)
            {
                alert("data not sent")
            }
            });
          edit.dialog("close" );
        }
      },
      close: function() {
        form[ 0 ].reset();
        allFields.removeClass( "ui-state-error" );
      }
    });
 
 //when submitted

    form = edit.find( "form" ).on( "submit", function( event ) {
      event.preventDefault();
      editUser();
    });
 
    
  } );


  </script>
  
<!-- dialog edit form html setup -->

  <div id="edit-form" title="Edit user">
  <form method = "post">
    <fieldset>
      <label for="img">Profile Picture</label>
      <input type="text" name="img" id="img" class="text ui-widget-content ui-corner-all">
      <label for="firstname">First Name</label>
      <input type="text" name="firstname" id="firstname" class="text ui-widget-content ui-corner-all">
      <label for="lastname">Last Name</label>
      <input type="text" name="lastname" id="lastname" class="text ui-widget-content ui-corner-all">
      <label for="email">Email</label>
      <input type="text" name="email" id="email" class="text ui-widget-content ui-corner-all">
      <label for="age">Age</label>
      <input type="text" name="age" id="age" class="text ui-widget-content ui-corner-all">
      <label for="location">Location</label>
      <input type="text" name="location" id="location" class="text ui-widget-content ui-corner-all">
      
 
      <!-- Allow form submission with keyboard without duplicating the dialog button -->
      <input type="submit" tabindex="-1" style="position:absolute; top:-1000px">
    </fieldset>
    <div id ="map3"></div>


  </form>
</div>



























<!-- style setup and jquery inclusions for creat new user dialog form -->

  

<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>


<!--js for create new user dialog form -->
<script>
  $( function() {
    var dialog, form,
 
      // From http://www.whatwg.org/specs/web-apps/current-work/multipage/states-of-the-type-attribute.html#e-mail-state-%28type=email%29
      emailRegex = /^[a-zA-Z0-9.!#$%&'*+\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/,
      img2 = $( "#img2" ),
      firstname2 = $( "#firstname2" ),
      lastname2 = $( "#lastname2"),
      email2 = $( "#email2" ),
      age2 = $( "#age2" ),
      location2 = $( "#location2"),
      lat2 = $( "#lat2"),
      lon2 = $("#lon2")
      allFields = $( [] ).add( img2 ).add( firstname2 ).add( lastname2 ).add( email2 ).add( age2 ).add( location2).add( lat2 ).add( lon2 ),
      tips = $( ".validateTips" );
 
    function updateTips( t ) {
      tips
        .text( t )
        .addClass( "ui-state-highlight" );
      setTimeout(function() {
        tips.removeClass( "ui-state-highlight", 1500 );
      }, 500 );
    }
 
 //function to make sure inputted values are valid
    function checkLength( o, n, min, max ) {
      if ( o.val().length > max || o.val().length < min ) {
        o.addClass( "ui-state-error" );
        updateTips( "Length of " + n + " must be between " +
          min + " and " + max + "." );
        return false;
      } else {
        return true;
      }
    }
 
 //additonal validity checks
    function checkRegexp( o, regexp, n ) {
      if ( !( regexp.test( o.val() ) ) ) {
        o.addClass( "ui-state-error" );
        updateTips( n );
        return false;
      } else {
        return true;
      }
    }
 
 //adding new user through ajax
    function addUser() {
      var valid = true;
      allFields.removeClass( "ui-state-error" );
 
      valid = valid && checkLength( firstname2, "firstname2", 1, 30 );
      valid = valid && checkLength( lastname2, "lastname2", 1, 30 );
      valid = valid && checkLength( email2, "email2", 6, 80 );
      valid = valid && checkLength( age2, "age2", 1, 10 );
      valid = valid && checkLength( location2, "location2", 1, 30 );
 
      valid = valid && checkRegexp( firstname2, /^[a-z]([0-9a-z_\s])+$/i, "Username may consist of a-z, 0-9, underscores, spaces and must begin with a letter." );
      valid = valid && checkRegexp( email2, emailRegex, "eg. ui@jquery.com" );
      valid = valid && checkRegexp( age2, /^([0-9a-zA-Z])+$/, "Password field only allow : a-z 0-9" );
 
      if ( valid ) {
        var user = [img2.val(), firstname2.val(), lastname2.val(), email2.val(), age2.val(), location2.val(), addlat, addlon];
        $.ajax({
            url : 'create3.php',
            method : 'POST',
            data:{
                arrayData:user
            },
            success : function(response)
            {
                alert("data sent response is "+response);
            },
            error : function(e)
            {
                alert("data not sent")
            }
            });
        dialog.dialog( "close" );
      }
      new google.maps.Marker({position: {lat: addlat, lng:addlon}, map: map});
      return valid;
    }

    

    
 //create new user dialog form style and buttons

    dialog = $( "#dialog-form" ).dialog({
      autoOpen: false,
      height: 400,
      width: 350,
      modal: true,
      buttons: {
        "Submit": addUser,
        Cancel: function() {
          dialog.dialog( "close" );
        }
      },
      close: function() {
        form[ 0 ].reset();
        allFields.removeClass( "ui-state-error" );
      }
    });
 
 //handling submit button click
    form = dialog.find( "form" ).on( "submit", function( event ) {
      event.preventDefault();
      addUser();
    });
 
 //handling create new user button click
    $( "#create-user" ).button().on( "click", function() {
      dialog.dialog( "open" );
    });
  } );

  
  </script>


<!-- html form setup for create new user dialog form -->
 
<div id="dialog-form" title="Create new user">
 
  <form method = "post">
    <fieldset>
      <label for="img2">Profile Picture</label>
      <input type="text" name="img2" id="img2" class="text ui-widget-content ui-corner-all">
      <label for="firstname2">First Name</label>
      <input type="text" name="firstname2" id="firstname2" class="text ui-widget-content ui-corner-all">
      <label for="lastname2">Last Name</label>
      <input type="text" name="lastname2" id="lastname2" class="text ui-widget-content ui-corner-all">
      <label for="email2">Email</label>
      <input type="text" name="email2" id="email2" class="text ui-widget-content ui-corner-all">
      <label for="age2">Age</label>
      <input type="text" name="age2" id="age2" class="text ui-widget-content ui-corner-all">
      <label for="location2">Location</label>
      <input type="text" name="location2" id="location2" class="text ui-widget-content ui-corner-all">
      
 
      <!-- Allow form submission with keyboard without duplicating the dialog button -->
      <input type="submit" tabindex="-1" style="position:absolute; top:-1000px">
    </fieldset>
<div id = "map2"></div>

  </form>

</div>





 



 <!--create new user button -->

<button id="create-user">Create new user</button>
 




    <h3>Map With Pins of all Users</h3>
    <!--The div element for the map -->
    <div id="map"></div>


    


    <script>
$(document).ready(function(){
 
// Initialize and add the map
$(function initMap() {
  for (var i = 0; i<rows; i++){
    lattt[i] = parseInt(lattt[i], 10);
    lonnn[i] = parseInt(lonnn[i], 10);
  }
  // The location of Uluru
  console.log(lattt);
  var strt = {lat: 46.7296, lng: -94.6859};
  // The map, centered at Uluru
  var map = new google.maps.Map(
      document.getElementById('map'), {zoom: 4, center: strt});
  // The marker, positioned at Uluru
  for (var i = 0; i < rows; i++){
  new google.maps.Marker({position: {lat: lattt[i], lng:lonnn[i]}, map: map});
  }



  var myLatlng = {lat: 46.7296, lng: -94.6859};

  //create new user dialog map

        var map2 = new google.maps.Map(document.getElementById('map2'), {
          zoom: 4,
          center: myLatlng
        });

        

        map2.addListener('click', function(event) {
          placeMarker(event.latLng);

        });

        function placeMarker(location) {
    var marker3 = new google.maps.Marker({
        position: location, 
        map: map2,
        draggable: true
    });
    var lati = marker3.getPosition().lat();
    var longi = marker3.getPosition().lng();
    addlat = lati;
    addlon = longi;
    lattt.push(addlat);
    lonnn.push(addlon);
}


 //edit user dialog map

 var map3 = new google.maps.Map(document.getElementById('map3'), {
          zoom: 4,
          center: myLatlng
        });


        map3.addListener('click', function(event) {
          placeMarker2(event.latLng);

        });

        function placeMarker2(location) {
    var marker4 = new google.maps.Marker({
        position: location, 
        map: map3,
        draggable: true
    });
    var lati = marker4.getPosition().lat();
    var longi = marker4.getPosition().lng();
    addlat = lati;
    addlon = longi;
    lattt.push(addlat);
    lonnn.push(addlon);
}


        
})
});



    </script>
  
  

    
   

    <?php include "templates/footer.php"; ?>

   